(function () {
	var JoystickControls = Object.seal({
		left: "left",
		right: "right",
		up: "up",
		down: "down"
	});
	var createKeysStateEvent = function (keysState) {
		return new CustomEvent("keysState", {
			detail: keysState,
			bubbles: true,
			cancelable: false
		});
	};
	var parseKeyToJoystick = function (key) {
		switch (key) {
		case 37: return JoystickControls.left;
		case 39: return JoystickControls.right;
		case 38: return JoystickControls.up;
		case 40: return JoystickControls.down;
		}
		return undefined;
	};

	var Component = function Component (width, height, color, x, y, screen) {
		var self = this;
		var gravityPower = 0.5;
		var resistancePower = 0.5;
		this.width = width;
		this.height = height;
		this.x = x;
		this.y = y;
		this.speedX = 0;
		this.speedY = 0;
		this.draw = function () {
			screen.context.fillStyle = color;
			screen.context.fillRect(self.x, self.y, self.width, self.height);
		};
		this.update = function() {
			self.x += self.speedX;
			self.y = self.speedY;
			self.speedX *= resistancePower;
			self.speedY += Math.max(5, gravityPower);
			self.jumping = self.speedY < 0;

			self.x = Math.min(screen.width - self.width, Math.max(0, self.x));
			self.y = Math.min(screen.height - self.height, Math.max(0, self.y));
			self.draw();
		};
	};
	
	var Game = function Game() {
		var self = this;
		this.__screen_canvas = document.getElementById("canvas");		
		this.__screen_context = this.__screen_canvas.getContext("2d");
		this.__screen_components = [];
		this.__screen_running = false;
		this.__screen_interval = null;
		
		Object.defineProperty(this, "canvas", {get: function() {
			return this.__screen_canvas;
		}});
		Object.defineProperty(this, "context", {get: function() {
			return this.__screen_context;
		}});
		Object.defineProperty(this, "running", {get: function() {
			return this.__screen_running;
		}});
		Object.defineProperty(this, "width", {get: function () {
			return window.innerWidth || (document.documentElement || document.body || {}).clientWidth;
		}});
		Object.defineProperty(this, "height", {get: function () {
			return window.innerHeight || (document.documentElement || document.body || {}).clientHeight;
		}});
		this.createComponent = function (width, height, color, x, y) {
			var component = new Component(width, height, color, x, y, {
				context: self.context,
				get width () {return self.width;},
				get height () {return self.height;},
				colides: self.collides
			});
			self.__screen_components.push(component);
			return component;
		};
		this.clear = function () {
			self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);
		};
		this.update = function () {
			if (self.__screen_onKeyEvent_keys) {
				var event = createKeysStateEvent(self.__screen_onKeyEvent_keys);
				window.dispatchEvent(event);
			}
			self.__screen_components.forEach(function (element) {
				element.update();
			});
		};
		this.start = function () {
			self.__screen_interval = setInterval(function () {
				self.__screen_running = true;
				self.clear();
				self.update();
			}, 20);
		};
		this.end = function () {
			if (!self.__screen_running || self.__screen_interval) return false;
			clearInterval(self.__screen_interval);
			self.__screen_interval = null;
			self.__screen_running = false;
			return true;
		};
		this.onKeysStateChange = function (callback) {
			if (!callback || !callback.apply) return;
			window.addEventListener("keysState", function (event) {
				callback.apply(undefined, [event.detail || {}]);
			}, false);
		};
		this.collides = function (x, y, width, height) {
			if (x < self.width) {
				return self.width
			}
		};

		this.__screen_canvas.width = this.width;
		this.__screen_canvas.height = this.height;
		window.addEventListener("keydown", function (e) {
			self.__screen_onKeyEvent_keys = self.__screen_onKeyEvent_keys || {};
			var inputValue = parseKeyToJoystick(e.keyCode);
			self.__screen_onKeyEvent_keys[inputValue] = true;
		});
		window.addEventListener("keyup", function (e) {
			var inputValue = parseKeyToJoystick(e.keyCode);
			if (self.__screen_onKeyEvent_keys && self.__screen_onKeyEvent_keys[inputValue]) {
				setTimeout(function () {
					delete self.__screen_onKeyEvent_keys[inputValue];
				}, 20);
			}
		});
		window.addEventListener("resize", function () {
			self.__screen_canvas.width = this.width;
			self.__screen_canvas.height = this.height;
		});
	};

	document.addEventListener("DOMContentLoaded", function () {
		var game = new Game();
		var player = game.createComponent(50, 50, "#ff3030", 0, 0);
		game.onKeysStateChange(function (joystick) {
			if (joystick.left) {
				player.speedX -= 1; 
			}
			if (joystick.right) {
				player.speedX += 1; 
			}
			if (joystick.up && !player.jumping) {
				player.speedY = -12;
				player.jumping = true; 
			}
			// if (joystick.down) {
			// 	player.speedY = 1; 
			// }
		});
		game.start();
	});
})();
